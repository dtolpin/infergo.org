---
title: "Getting started"
weight: 10
---

## Prerequisites

`infergo` benefits from
[modules](https://github.com/golang/go/wiki/Modules) introduced
in Go 1.11, and relies on
[`go/packages`](https://godoc.org/golang.org/x/tools/go/packages)
to import packages in a module-aware way, but will work with
earlier versions of Go.  [Install
Go](https://golang.org/doc/install). It is easier to build and
use `infergo` with the `make` utility.

## Installation

There are two installation options:

1. If your Go project imports any of [`infergo`
   packages](https://godoc.org/bitbucket.org/dtolpin/infergo) and
   uses modules, `infergo` will be installed for you. `deriv`
   utility will be in `$GOPATH/bin`. As a side effect of
   providing examples with the main repository, the example
   binaries will also be installed in `$GOPATH/bin`. They are
   not needed there, you can remove them.

2. Alternatively, you can clone the
   [repository](https://bitbucket.org/dtolpin/infergo) and build
   `infergo` from the cloned directory:
```
git clone https://bitbucket.org/dtolpin/infergo
cd infergo
make install
```
  This will install only `deriv` but not any example binaries.
  To build examples (and run each of them on the embedded
  self-check dataset), run
```
make examples
```

## Hello world

The [probabilistic "Hello
world"](https://bitbucket.org/dtolpin/infergo/src/master/examples/hello)
example from the `infergo` repository shows a typical project
layout and commands to build a Go program with an `infergo`
model. Explore the example's source code and
[Makefile](https://bitbucket.org/dtolpin/infergo/src/master/examples/hello/Makefile).
In a nutshell, one needs to:

1. implement the model in a separate package,
2. differentiate the model's package with `deriv`,
3. import the package with the differentiated model (ending in
  `"ad/"`) in the file where inference is performed,
4. build the project in the normal Go way.
