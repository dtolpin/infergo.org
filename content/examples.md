---
title: "Examples"
weight: 60
---

`infergo` comes bundled with [basic
examples](https://bitbucket.org/dtolpin/infergo/src/master/examples/).
An example is a good starting point for a new `infergo`
project.

Advanced examples and case studies go into a separate
repository,
[https://bitbucket.org/dtolpin/infergo-studies](https://bitbucket.org/dtolpin/infergo-studies).
