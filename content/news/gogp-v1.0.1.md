---
Title: "gogp v1.0.1"
date: 2021-07-11T18:07:04+03:00
author: "David Tolpin"
draft: False
---

[GoGP v1.0.1](/peers/gogp) is
[out](http://bitbucket.org/dtolpin/gogp/srv/v1.0.1). This is the
first stable (v1) release of GoGP, a library for Gaussian
process regression. GoGP has been used in production for over a
year, and has undergone many changes improving performance and
robustness.
