---
Title: "gogp v0.1.0"
date: 2020-01-24T01:17:00+02:00
author: "David Tolpin"
draft: False
---

[GoGP](/peers/gogp) is [out](http://bitbucket.org/dtolpin/gogp).
GoGP is a library for Gaussian process regression in Go and uses
Infergo for automatic differentiation and inference.
