---
Title: "infergo v1.0.1"
date: 2021-07-11T18:07:04+03:00
author: "David Tolpin"
draft: False
---

Infergo v1.0.1 is [out](https://bitbucket.org/dtolpin/infergo/src/v1.0.1/).

This is the first stable (v1) release of Infergo. Infergo has
undergone many changes during the past year, and has been
used in production for mission-critical computations in the
cloud.
